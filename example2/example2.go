package main

import (
	"fmt"
	"os"
	"strconv"
)

func doPartition(numbers []int, pivot int) (minors []int, largers []int) {
	for _, n := range numbers {
		if n <= pivot {
			minors = append(minors, n)
		} else {
			largers = append(largers, n)
		}
	}
	return minors, largers
}

func quicksort(numbers []int) []int {
	if len(numbers) <= 1 {
		return numbers
	}

	n := make([]int, len(numbers))
	copy(n, numbers)

	pivotIndex := len(n) / 2
	pivot := n[pivotIndex]

	n = append(n[:pivotIndex], n[pivotIndex+1:]...)

	minors, largers := doPartition(n, pivot)

	return append(
		append(quicksort(minors), pivot),
		quicksort(largers)...)
}

func main() {
	entry := os.Args[1:]
	numbers := make([]int, len(entry))

	for i, v := range entry {
		number, err := strconv.Atoi(v)
		if err != nil {
			fmt.Printf("%s não é um número válido", v)

			os.Exit(1)
		}

		numbers[i] = number
	}

	fmt.Println(quicksort(numbers))
}
