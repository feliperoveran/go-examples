package main

import (
	"fmt"
	"os"
	"strconv"
)

func validateArgs(args []string) {
	if len(args) < 3 {
		fmt.Println("Uso: example1.go <valores> <unidade>")
		os.Exit(1)
	}
}

func parseDestUnit(sourceUnit string) string {
	var destUnit string

	if sourceUnit == "celsius" {
		destUnit = "fahrenheit"
	} else if sourceUnit == "kilometros" {
		destUnit = "milhas"
	} else {
		fmt.Printf("%s não é uma unidade conhecida\n", sourceUnit)
		os.Exit(1)
	}

	return destUnit
}

func convertValues(sourceUnit string, sourceValue float64) float64 {
	var destValue float64

	if sourceUnit == "celsius" {
		destValue = sourceValue*1.8 + 32
	} else {
		destValue = sourceValue / 1.60934
	}

	return destValue
}

func main() {
	validateArgs(os.Args)

	sourceUnit := os.Args[len(os.Args)-1]
	sourceValues := os.Args[1 : len(os.Args)-1]
	destUnit := parseDestUnit(sourceUnit)

	for i, v := range sourceValues {
		sourceValue, err := strconv.ParseFloat(v, 64)

		if err != nil {
			fmt.Printf("O valor %s na posição %d não é um número válido!\n", v, i)
			os.Exit(1)
		}

		destValue := convertValues(sourceUnit, sourceValue)
		fmt.Printf("%.2f %s = %.2f %s\n", sourceValue, sourceUnit, destValue, destUnit)
	}
}
