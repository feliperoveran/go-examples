package main

import (
	"errors"
	"fmt"
)

// Stack is a stack
type Stack struct {
	values []interface{}
}

// Size returns the stack size
func (stack Stack) Size() int {
	return len(stack.values)
}

// Empty returns true when the stack is empty. Otherwise returns false
func (stack Stack) Empty() bool {
	return stack.Size() == 0
}

// Push pushes values to the stack
func (stack *Stack) Push(value interface{}) {
	stack.values = append(stack.values, value)
}

// Pop pops values from the stack
func (stack *Stack) Pop() (interface{}, error) {
	if stack.Empty() {
		return nil, errors.New("stack is empty")
	}

	value := stack.values[stack.Size()-1]
	stack.values = stack.values[:stack.Size()-1]

	return value, nil
}

func main() {
	stack := Stack{}
	fmt.Println("Stack created with size ", stack.Size())
	fmt.Println("Stack empty? ", stack.Empty())

	stack.Push("Go")
	stack.Push(1234)
	stack.Push(3.14)
	stack.Push("End")

	fmt.Println("Stack size after pushing 4 values: ", stack.Size())
	fmt.Println("Stack empty? ", stack.Empty())

	for !stack.Empty() {
		v, _ := stack.Pop()
		fmt.Println("Popping ", v)
		fmt.Println("Stack size: ", stack.Size())
		fmt.Println("Stack empty? ", stack.Empty())
	}

	_, err := stack.Pop()
	if err != nil {
		fmt.Println("Error: ", err)
	}
}
