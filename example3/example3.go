package main

import (
	"fmt"
	"os"
	"strings"
)

func getStats(words []string) map[string]int {
	stats := make(map[string]int)

	for _, word := range words {
		startLetter := strings.ToUpper(string(word[0]))

		counter, found := stats[startLetter]

		if found {
			stats[startLetter] = counter + 1
		} else {
			stats[startLetter] = 1
		}
	}

	return stats
}

func printStats(stats map[string]int) {
	fmt.Println("Contagem de palavras iniciadas com cada letra:")

	for startLetter, stat := range stats {
		fmt.Printf("%s = %d\n", startLetter, stat)
	}
}

func main() {
	words := os.Args[1:]
	stats := getStats(words)
	printStats(stats)
}
