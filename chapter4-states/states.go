package main

import "fmt"

// State defines a State with name, population and capital
type State struct {
	name       string
	population int
	capital    string
}

func main() {
	states := make(map[string]State, 5)

	states["GO"] = State{"Goiás", 6434052, "Goiânia"}
	states["PB"] = State{"Paraíba", 3914418, "João Pessoa"}
	states["PR"] = State{"Paraná", 10997462, "Curitiba"}
	states["RN"] = State{"Rio Grande do Norte", 3373960, "Natal"}
	states["SE"] = State{"Sergipe", 2228489, "Aracaju"}

	fmt.Println(states)
}
